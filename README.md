prometheus-airnow-exporter
==========================

> This project is no longer maintained.

A Prometheus exporter for AQI readings from AirNow.gov.

You will need to obtain your own API key from [AirNow](https://docs.airnowapi.org/).

Basic usage:
```
API_KEY=... ZIP_CODE=12345 ./prometheus-airnow-exporter
```

By default, the exporter will be available at http://localhost:42515/metrics.
You can change the port by setting the `PORT` environment variable.

Released under the GPL v3, or any later version.
