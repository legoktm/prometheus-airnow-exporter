/**
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::Result;
use prometheus_exporter::prometheus::{labels, register_int_gauge_vec};
use serde::Deserialize;
use tokio::time::{sleep, Duration};
use tracing::{debug, error, info};
use tracing_subscriber::EnvFilter;

/// An observation by zip code. See [upstream docs](https://docs.airnowapi.org/CurrentObservationsByZip/docs)
/// for details on individual field meanings.
#[derive(Deserialize, Debug)]
struct Observation {
    // #[serde(rename = "DateObserved")]
    // date_observed: String,
    // #[serde(rename = "HourObserved")]
    // hour_observed: u32,
    // #[serde(rename = "LocalTimeZone")]
    // local_time_zone: String,
    #[serde(rename = "ReportingArea")]
    reporting_area: String,
    // #[serde(rename = "StateCode")]
    // state_code: String,
    // #[serde(rename = "Latitude")]
    // latitude: f32,
    // #[serde(rename = "Longitude")]
    // longitude: f32,
    #[serde(rename = "ParameterName")]
    parameter_name: String,
    #[serde(rename = "AQI")]
    aqi: u32,
    // #[serde(rename = "Category")]
    // category: Category,
}

/*
#[derive(Deserialize, Debug)]
struct Category {
    #[serde(rename = "Number")]
    number: u32,
    #[serde(rename = "Name")]
    name: String,
}
 */

async fn fetch(api_key: &str, zip_code: &str) -> Result<Vec<Observation>> {
    debug!("Fetching new data from AirNow");
    let client = reqwest::Client::new();
    let obversations = client
        .get("https://www.airnowapi.org/aq/observation/zipCode/current/")
        .query(&[
            ("format", "application/json"),
            ("distance", "25"),
            ("zipCode", zip_code),
            ("API_KEY", api_key),
        ])
        .send()
        .await?
        .json()
        .await?;
    info!("Received new data from AirNow");
    Ok(obversations)
}

async fn run(api_key: &str, zip_code: &str) {
    let gauge_vec = register_int_gauge_vec!(
        "airnow_aqi",
        "AQI from AirNow.gov",
        &["type", "zipcode", "reporting_area"]
    )
    .unwrap();
    loop {
        let observations = match fetch(api_key, zip_code).await {
            Ok(observations) => observations,
            Err(err) => {
                error!("Error fetching, waiting 60s: {}", err);
                sleep(Duration::from_secs(60)).await;
                continue;
            }
        };
        for observation in observations {
            let gauge = gauge_vec
                .get_metric_with(&labels! {
                    "type" => observation.parameter_name.as_str(),
                    "zipcode" => zip_code,
                    "reporting_area" => observation.reporting_area.as_str(),
                })
                .unwrap();
            gauge.set(observation.aqi as i64);
        }
        sleep(Duration::from_secs(60 * 15)).await;
    }
}

#[tokio::main]
async fn main() {
    tracing_subscriber::FmtSubscriber::builder()
        .with_env_filter(EnvFilter::new(
            // Default to RUST_LOG=info if not explicitly set
            std::env::var("RUST_LOG").unwrap_or_else(|_| "info".to_string()),
        ))
        .init();
    tokio::task::spawn_blocking(|| {
        let port =
            std::env::var("PORT").unwrap_or_else(|_| "42515".to_string());
        prometheus_exporter::start(
            format!("0.0.0.0:{}", &port)
                .parse()
                .expect("invalid PORT specified"),
        )
        .unwrap();
    });
    let api_key = std::env::var("API_KEY")
        .expect("No/invalid API_KEY environment variable set");
    // TODO: support multiple zipcodes, via CLI arguments
    let zip_code = std::env::var("ZIP_CODE")
        .expect("No/invalid ZIP_CODE environment variable set");
    run(&api_key, &zip_code).await;
}
